import numpy

class Instrument:
    def __init__(self, name, basePrice, drift, variance):
        self.name = name
        self.__startingPrice = basePrice
        self.__price = basePrice
        self.__drift = drift
        self.__variance = variance

    def getPrice (self):
        return self.__price

    def getDrift(self):
        return self.__drift

    def getVariance(self):
        return self.__variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0,1)*self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__startingPrice*0.4:
            self.__drift =( -0.7 * self.__drift )
        self.__price = newPrice*1.01 if direction=='B' else newPrice*0.99
        return self.__price