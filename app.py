import time
from flask import Flask, Response
from flask_cors import CORS, cross_origin
import time
import numpy, random
from datetime import datetime, timedelta
import json
from Instrument import Instrument
from datagen import instruments, counterparties, NUMBER_OF_RANDOM_DEALS, TIME_PERIOD_MILLIS, EPOCH, RandomDealData

app = Flask(__name__)
CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')
def hello():
    return 'Hi!'


@app.route('/deals/stream')
@cross_origin()
def stream():
    InstrList = RandomDealData.createInstrumentList()
    def eventStream():
        while True:
            # wait for source data to be available, then push it
            time.sleep(1)
            yield 'data: ' + RandomDealData.createRandomData(InstrList)+"\n\n"

    return Response(eventStream(), mimetype="text/event-stream")


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)





# def publish(self, event, *args, **kwargs):
#        threads = []
#        if event in self.subscribers.keys():
#            for callback in self.subscribers[event]:
#                threads.append(threading.Thread(
#                  target=callback,
#                  args=args,
#                  kwargs=kwargs
#                ))
#            for th in threads:
#                th.start()
#
#            if self.blocking:
#                for th in threads:
#                    th.join()

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))